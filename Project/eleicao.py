total_vagas = 29
QE = 12684


import pandas as pd
#Abrindo o arquivo eleicao.csv e criando um dataFrame
data_main = pd.read_csv('eleicao.csv', sep=';')
print(data_main.head())
part_colig = list(set(data_main['Partido/Coligação']))


#Determinando coligacoes
coligacoes = []
for i in data_main['Partido/Coligação']:
    list2 = i.split(' - ')
    if len(list2) == 2:
        coligacoes.append(list2[1])
    else:
        coligacoes.append(list2[0])
        
data_main['Coligação Only'] = coligacoes

coligacoes = list(set(coligacoes))
list_inicial = [0]*len(coligacoes)
new_Data = {'Coligações':coligacoes,
             'Votos':list_inicial,
             'Qtd Vagas': list_inicial,
             'Media':list_inicial,
             'Vagas Residuais': list_inicial,
            'Total Vagas':list_inicial}
            
new_DataFrame = pd.DataFrame(new_Data)
i = 0
#Quantidade de votos cada Coligação
for str in coligacoes:
    sum_votos = (data_main.loc[(data['Coligação Only'] == str) & (data_main['Votos'] >= 0), ['Votos']]).sum()
    new_DataFrame.at[i, 'Votos'] =  int(sum_votos) # Somatoria de votos
    new_DataFrame.at[i, 'Qtd Vagas'] =  sum_votos//QE # Quoeficiente Partidario 
    i+=1
#Vagas Residuais
vagas_restantes = total_vagas - new_DataFrame['Qtd Vagas'].sum()

#Calcular media
for i in range(0, vagas_restantes):
    for str in new_DataFrame['Coligações']:
        sum_votos = int(new_DataFrame.loc[(new_DataFrame['Coligações'] == str) & (new_DataFrame['Qtd Vagas'] > 0), ['Votos']].sum())
        qtd_vagas = int(new_DataFrame.loc[(new_DataFrame['Coligações'] == str) & (new_DataFrame['Qtd Vagas'] > 0), ['Qtd Vagas']].sum())
        vagas_res = int(new_DataFrame.loc[(new_DataFrame['Coligações'] == str) & (new_DataFrame['Qtd Vagas'] > 0), ['Vagas Residuais']].sum())
        media = sum_votos//(qtd_vagas+(vagas_res)+1)
        new_DataFrame.loc[new_DataFrame['Coligações'] == str,'Media'] = media
    new_DataFrame.loc[new_DataFrame['Media'].idxmax(), 'Vagas Residuais'] += 1 # A coligação com maior media, ganha uma vaga residual

for str in new_DataFrame['Coligações']:
    qtd_vagas = int(new_DataFrame.loc[new_DataFrame['Coligações'] == str,'Qtd Vagas'])
    vagas_res = int(new_DataFrame.loc[new_DataFrame['Coligações'] == str,'Vagas Residuais'])
    new_DataFrame.loc[new_DataFrame['Coligações'] == str,'Total Vagas'] = qtd_vagas + vagas_res
new_DataFrame.sort_values(by='Total Vagas', ascending=False)
#Candidatos Eleitos
sorted_data_main = data_main.sort_values(by='Votos', ascending = False)
list_eleitos = []
for str in coligacoes:
    partido = sorted_data_main[sorted_data_main['Coligação Only'] == str]
    n_cand =  int(new_DataFrame.loc[new_DataFrame['Coligações'] == str,'Total Vagas'])
    list_eleitos.append(partido.iloc[0:n_cand])

       
eleitos = pd.concat(list_eleitos).sort_values(by='Votos', 
                                              ascending=False).drop('Coligação Only', axis=1)

eleitos.to_csv('resultado.tsv', sep='\t', encoding='utf-8', index = False, header=False)
eleitos